#!/usr/bin/env ruby
require 'optparse'
require 'ostruct'
VERSION = 0.01

def parse_args(args)
    options = OpenStruct.new
    options.sleep = 5
    options.ifcs = []

    opts = OptionParser.new do |opts|
        opts.banner  = "Usage: bandwidth.rb [options] [-- progname progoptions]"
        opts.separator ""
        opts.separator "If a process is given, monitor its bandwidth usage. Use -- to separate process args from script args."
        opts.separator "If no process given, continuously monitor system bandwidth."
        opts.separator "If no interface is specified, all interfaces are monitored. Multiple --iface args accepted."
        opts.separator "Add :tx or :rx to an interface name if you only want that direction."
        opts.separator ""
        opts.separator "Specific options:"

        opts.on("-i", "--iface IFACE", "monitor a specific interface") {|iface| options.ifcs << iface }
        opts.on("-s", "--sleep SECONDS", "sleep interval when polling") {|sl| options.sleep = sl.to_i }
        opts.separator ""
        opts.separator "Common options:"
        opts.on_tail("-h", "--help", "show help") { puts opts }
        opts.on_tail("-v", "--version", "show version") { puts VERSION }
    end
    opts.parse!(args)
    options
end

def gather_stats(info=nil)
    info ||= Hash.new {|hash, key| hash[key] = { :bw => [], :stats => [] } }
    File.open('/proc/net/dev') do |f|
        f.each_line do |l|
            if m = l.match(/(\S+):\s*(\d+)\s*(\d+\s*){7,7}(\d+)/)
                info[m[1]][:stats].push [ Time.now.to_i, m[3].to_i, m[2].to_i ] # time, tx bytes, rx bytes
            end
        end
    end
    info
end

def show_stats(ifcs, want_ifcs=[])
    ifcs.each do |i,v|
        f,l = v[:stats].first, v[:stats].last
        return unless (dt = l[0] - f[0]) > 0

        v[:bw].push([ (l[1] - f[1]) / dt, (l[2] - f[2]) / dt ])  # tx bw, rx bw
        puts "#{i}: tx #{v[:bw].last[0]} Bps, rx #{v[:bw].last[1]} Bps" if want_ifcs.empty? || want_ifcs.include?(i) 
        puts "#{i}: tx #{v[:bw].last[0]} Bps" if want_ifcs.include? "#{i}:tx"
        puts "#{i}: rx #{v[:bw].last[1]} Bps" if want_ifcs.include? "#{i}:rx"
    end
    puts ""
end

# MAIN
options = parse_args(ARGV)
ifcs = gather_stats

if ARGV.empty?
    loop do
        gather_stats(ifcs)
        show_stats(ifcs, options.ifcs)
        sleep options.sleep
    end
else
    system(*ARGV)
    gather_stats(ifcs)
    show_stats(ifcs, options.ifcs)
end



