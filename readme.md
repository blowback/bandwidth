bandwidth.rb -- bandwidth monitoring script
===========================================

bandwidth.rb monitors the transmit and receive bandwidth for all interfaces on your system. It can either do this indefinitely until you interrupt it, or it can measure the bandwidth over the duration of a specified process.

bandwidth.rb measures true bytes-on-the-wire TX and RX bandwidth, so the figure you get includes all protocol framing, TCP/IP overheads, etc.

bandwidth.rb currently reports in bytes per second. No doubt in the future it will accomodate kibibytes and all that crap.

Continuous monitoring mode
--------------------------

    ./bandwidth.rb [--sleep SECS] [--iface X[:tx|:rx]] [--iface ...]

To run in continuous mode, call the script as `./bandwidth.rb`. It will report every five seconds. If you want to change the reporting interval, use the `--sleep SECS` option. 

If you're only interested in the eth1 interface, use `--iface eth1`. If you're only interested in RX bandwidth for eth2, use `--iface eth2:rx`. If you're interested in TX bandwidth for eth1 and RX bandwidth for eth2, use `--iface eth1:tx --iface eth2:rx`.

Process monitoring mode
-----------------------

    ./bandwidth.rb [--iface X[:tx|:rx]]... -- prog_to_run prog_args

To monitor the bandwidth consumed by running a command, use the above form. Be sure to include the '--' if your command has its own arguments, otherwise bandwidth.rb will try to consume them.

You can use `--iface` options, exactly as detailed above.

For example, downloading a linux kernel on my system:

    ./bandwidth.rb --iface eth1:rx -- wget -q -O /dev/null http://kernel.ubuntu.com/~kernel-ppa/mainline/v3.4.4-quantal/linux-image-extra-3.4.4-030404-generic_3.4.4-030404.201206221555_i386.deb 

    eth1: rx 9494556 Bps

Note the '--'!

